import React, {useEffect, useState} from 'react';

import Intro from './intro';
import Contact from './contact';
import Sale from './sale';
import {useParams, useHistory} from 'react-router-dom';
import constant from "../../helpers/constant";

const model = 'single';

const Index = () => {
    const [page, setPage] = useState();
    const {slug} = useParams();
    const history = useHistory();
    useEffect(() => {
        handleChangePage(slug);
    }, [slug]);
    const handleChangePage = (page, data = {}) => {
        setPage(page);
    };
    const handleRedirect = () => {
        history.push(constant.ROUTES.BE.HOME);
    };
    switch (page) {
        case 'intro':
            return <Intro model={model}
                          handleChangePage={handleChangePage}
                          handleRedirect={handleRedirect}


            />;
        case 'contact':
            return <Contact model={model}
                            handleChangePage={handleChangePage}
                            handleRedirect={handleRedirect}


            />;
        case 'sale':
            return <Sale model={model}
                         handleChangePage={handleChangePage}
                         handleRedirect={handleRedirect}
            />;
        default:
            return ''
    }
};

export default Index;
