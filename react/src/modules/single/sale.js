import React, { Component } from 'react'
import {
    Button, Header, Segment,

} from 'semantic-ui-react'
import {
    Form, Input, TextArea
} from 'formsy-semantic-ui-react';
import models from '../../models/models';
import _ from 'lodash';
const type ='sale';
class Intro extends Component {
    state = {
        page: {},
    };
    componentDidMount() {
        const {model} = this.props;
        models[model].get({type: type}).then((res) => {
            if(!_.isEmpty(res)) {
                this.setState({
                    page: res? res[0]: {}
                })
            }
        })
    }

    onValidSubmit = (data) => {
        const {handleRedirect, model} = this.props;
        const {page} = this.state;

        if (_.isEmpty(page)) {
            models[model].post(data).then((res) => {
                if (!res) {
                    alert('Created failed')
                } else {
                    handleRedirect('main');
                }
            })
        } else {
            models[model].put({type: type}, _.omit(data, 'type')).then((res) => {
                if (!res) {
                    alert('Update failed')
                } else {
                    handleRedirect('main');
                }
            })
        }
    };


    render() {
        const {handleRedirect} = this.props;
        const {page} = this.state;

        return (
            <div>
                <Header as='h2' attached='top'>
                    Sale Page
                </Header>
                <Segment attached>
                    <Form size="big" onSubmit={this.onValidSubmit}>
                        <Form.Input name="type" type="hidden" value={type}/>
                        <Form.Field
                            control={Input}
                            label='Title'
                            name="title"
                            placeholder='Title'
                            value={page.title}
                        />
                        <Form.Field
                            control={TextArea}
                            label='Content'
                            name="content"
                            placeholder='Content'
                            value={page.content}
                        />

                        <Button color='grey' onClick={() => handleRedirect()}>Back Home</Button>
                        <Button color='teal' type="submit">Submit</Button>
                    </Form>
                </Segment>

            </div>

        )
    }
}

export default Intro