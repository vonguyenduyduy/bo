import React, { Component } from 'react'
import {
    Button,

} from 'semantic-ui-react'
import {
    Form, Input, TextArea, Select
} from 'formsy-semantic-ui-react';
import models from '../../models/models';
import CONSTANT from '../../helpers/constant';
import _ from 'lodash';
import FileManager from "../../helpers/ui/FileManager";
import Editor from '../../helpers/ui/Editor';

const type = [
    { key: 'o', text: '-- Select --', value: '' },
    { key: 'm', text: 'Hot', value: 'hot' },
    { key: 'f', text: 'Sale', value: 'sale' },
];

class Detail extends Component {
    state = {
        imageName: {}
    };
    editorContent = '';

    componentDidMount() {
        const {data} = this.props;
        this.setState({
            imageName: data.image
        });
    }
    handleEditorContent = (content) => {
        this.editorContent = content;
    };
    onValidSubmit = async (data) => {
        const {handleChangePage, add, model} = this.props;
        const {imageName} = this.state;

        try{
            if (!_.isEmpty(imageName)) {
                data.image = imageName;
            }
            data.content = this.editorContent;

            if (add) {
                await models[model].post(data).then((res) => {
                    if (!res) {
                        alert('Created failed')
                    } else {
                        handleChangePage('main');
                    }
                })
            } else {

                await models[model].put({_id: data._id}, _.omit(data, '_id')).then((res) => {
                    if (!res) {
                        alert('Update failed')
                    } else {
                        handleChangePage('main');
                    }
                })
            }
            return true;
        } catch (e) {
            console.log(e)
        }
    };
    onHasFile = (files) => {
        this.setState({
            files: files
        })
    };
    categoryOptions = (options) => {
        const rs = options.map((c) => {
            return {
                key: c._id,
                text: c.title,
                value: c._id,
            };
        })
        return rs;
    };
    onHasClick = (dir) => {
        this.setState({
            imageName: dir
        })
    };

    render() {
        const {imageName} = this.state;
        const {handleChangePage, data, category} = this.props;
        if (_.isEmpty(category)) {
            return 'Please Add Category firstly'
        }
        return (
            <div>
                {!_.isEmpty(data) && (
                    <h2>{data._id}</h2>
                )}
                <Form size="big" onSubmit={this.onValidSubmit}>
                    <Form.Input name="_id" type="hidden" value={data._id}/>
                    <Form.Group>
                        <Form.Field
                            control={Input}
                            label='Title'
                            name="title"
                            placeholder='Title'
                            value={data.title}
                            width={10}

                        />
                        <Form.Field width={6}>
                            <label>Image</label>
                            <FileManager image={imageName} action={false} onHasClick={this.onHasClick}/>
                        </Form.Field>

                    </Form.Group>
                    <Form.Group>
                        <Form.Field
                            control={Select}
                            name="category"
                            label='Category'
                            options={this.categoryOptions(category)}
                            placeholder='Category'
                            width={10}
                            value={data.category}
                        />
                    </Form.Group>
                    <Form.Group>
                        <Form.Field
                            control={Input}
                            label='Price'
                            name="price"
                            placeholder='price'
                            value={data.price}
                            width={3}
                        />
                        <Form.Field
                            control={Select}
                            name="status"
                            label='Status'
                            options={CONSTANT.OPTIONS.STATUS}
                            placeholder='Status'
                            width={3}
                            value={data.status}
                        />
                        <Form.Field
                            control={Select}
                            name="type"
                            label='Type'
                            options={type}
                            placeholder='type'
                            width={4}
                            value={data.type}
                        />



                    </Form.Group>
                    <Editor data={data.content} handleNewContent={this.handleEditorContent}/>
                    <Button color='grey' onClick={() => handleChangePage('main')}>Back</Button>
                    <Button color='teal' type="submit">Submit</Button>
                </Form>
            </div>

        )
    }
}

export default Detail