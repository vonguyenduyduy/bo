import React from 'react';

import Content from './Content'
import Detail from './Detail'


class Home extends React.Component {

    state = {
        home: false,
        detail: false
    };

    handleChangePage = (page, data = {}) => {
        this.setState({
            home: page === 'home',
            edit: page === 'edit',
            add: page === 'add',
            data : data
        })
    };

    render() {
        const {edit, data, add} = this.state;

        if (edit) {
            return <Detail add={add} data={data} handleChangePage={this.handleChangePage}/>
        }else if(add) {
            return <Detail add={add} data={data} handleChangePage={this.handleChangePage}/>
        } else {
            return <Content data={data} handleChangePage={this.handleChangePage}/>
        }
    }
}

export default Home;
