import React from 'react';

import {Grid, Container, Image, List, Header, Divider, Segment} from 'semantic-ui-react'
import {Link} from "react-router-dom";
import lang from "../../../helpers/lang";
import constant from "../../../helpers/constant";

class Content extends React.Component{
    componentDidMount() {
        this.init();
    }

    init = () => {

    };

    render() {
        return (
            <Segment className="bg-base" style={{ margin: '5em 0em 0em', padding: '5em 0em'}}>
                <Container textAlign='center'>
                    <Image centered size='mini'
                           src='https://www.nuockhoangviet.vn/gallery_gen/490f7a8dffea9144443295d5617b234d_140x70.jpg'
                    />
                    <List horizontal  divided link size='small'>
                        <List.Item as={Link} to={`${constant.ROUTES.FE.SINGLE}/intro`}>
                            {lang('intro')}
                        </List.Item>
                        <List.Item as={Link} to={`${constant.ROUTES.FE.SINGLE}/sale`}>
                            {lang('sale')}
                        </List.Item>
                        <List.Item as={Link} to={`${constant.ROUTES.FE.SINGLE}/contact`}>
                            {lang('contact')}
                        </List.Item>
                    </List>
                    <Header as="h4" style={{color:"#fff"}} color="white">©Copyright 06/2021 - Z.E.T Co.</Header>
                </Container>
            </Segment>
        );
    }
}

export default Content;
