import React, {useEffect, useState} from 'react';

import {Menu, Image, Card, Label} from 'semantic-ui-react'
import _ from 'lodash';
import {Link} from "react-router-dom";
import constant from "../../../helpers/constant";
import lang from "../../../helpers/lang";
import model from "../../../models/models";
import utils from "../../../helpers/utils";
import {Table} from "semantic-ui-react/dist/commonjs/collections/Table";

const Sidebar = (props) => {
    const {category} = props;
    const [list, setList] = useState([]);
    useEffect(() => {
        getList();
    }, [list]);
    const getList = () => {
        model.brand.getActive().then((res) => {
           setList(res)
        });
    };
    return (
        <>
            <Label size='huge' basic color='teal' ribbon>
                {lang('category')}
            </Label>
            <Menu pointing vertical size='large'>
                {!_.isEmpty(category) && category.map((row, index) => (
                        <Menu.Item
                            as={Link}
                            key={index}
                            name={row.title}
                            to={`${constant.ROUTES.FE.CATEGORY}/${row.title}/${row._id}`}
                        />
                    )
                )}
            </Menu>
            <Label size='huge' basic color='teal' ribbon>
                {lang('coop_brand')}
            </Label>
            {
                !_.isEmpty(list) && list.map((row) => (
                    <Card>
                        <Image src={utils.parseImage(row.image)} wrapped />
                    </Card>
                ))
            }
        </>
    );
};

export default Sidebar;
