import React from 'react';

import {Menu, Container, Image, Dropdown} from 'semantic-ui-react'
import _ from 'lodash';
import {Link} from "react-router-dom";
import constant from "../../../helpers/constant";
import lang from "../../../helpers/lang";

const Header = (props) => {
    const {category} = props;
    return (
        <Menu fixed='top' className="bg-base">
            <Container>
                <Menu.Item as={Link} header to='/'>
                    <Image size='mini'
                           src='https://www.nuockhoangviet.vn/gallery_gen/490f7a8dffea9144443295d5617b234d_140x70.jpg'
                           style={{marginRight: '1.5em'}}/>
                    Water
                </Menu.Item>
                <Menu.Item as={Link} to={`${constant.ROUTES.FE.SINGLE}/intro` }>{lang('intro')}</Menu.Item>

                <Dropdown item simple text={lang('category')}>
                    <Dropdown.Menu>
                        {!_.isEmpty(category) && category.map((row, index) => (
                            <Dropdown.Item as={Link} key={index} to={`${constant.ROUTES.FE.CATEGORY}/${row.title}/${row._id}`}>{row.title}</Dropdown.Item>
                        ))}
                    </Dropdown.Menu>
                </Dropdown>
                <Menu.Item as={Link} to={`${constant.ROUTES.FE.SINGLE}/contact` }>{lang('contact')}</Menu.Item>

            </Container>
        </Menu>
    );
}

export default Header;
