import React from 'react';

import {Header, Segment} from 'semantic-ui-react'
import models from "../../../models/models";
const type = 'sale';
class Intro extends React.Component{
    componentDidMount() {
        document.title = 'Sale';
        this.getData();
    }
    state = {
        page : {}
    };
    getData = () => {
        models.single.get({type: type}).then((res) => {
            this.setState({
                page: res? res[0]: {}
            })
        })
    };

    render() {
        const {page} = this.state;
        return (
            <div className="content">
                <Header as='h2' attached='top'>
                    Sale Page
                </Header>
                <Segment attached>
                    {page.content}
                </Segment>
            </div>
        );
    }
}

export default Intro;
