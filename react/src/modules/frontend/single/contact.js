import React from 'react';

import {Header, Segment, Card, Image} from 'semantic-ui-react'
import models from "../../../models/models";
const type = 'contact';
class Intro extends React.Component{
    componentDidMount() {
        document.title = 'Contact';
        this.getData();
    }
    state = {
        page : {}
    };
    getData = () => {
        models.single.get({type: type}).then((res) => {
            this.setState({
                page: res? res[0]: {}
            })
        })
    };

    render() {
        const {page} = this.state;
        return (
            <div className="content">
                <Header as='h2' attached='top' color="teal">
                    Contact Page
                </Header>
                <Segment attached>
                    <Card fluid color="teal">
                        <Image src='https://shoponix.envytheme.com/_next/static/media/furniture-bg.48c2145dca988d203a16902c97d9cab0.jpg' wrapped ui={false} />
                        <Card.Content>
                            <Card.Header>Daniel</Card.Header>
                            <Card.Meta>Address: {page.address}</Card.Meta>
                            <Card.Description>
                                Email: {page.email}
                            </Card.Description>
                        </Card.Content>
                        <Card.Content extra>
                            Phone: {page.phone}
                        </Card.Content>
                    </Card>
                </Segment>
            </div>
        );
    }
}

export default Intro;
