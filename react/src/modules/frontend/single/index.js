import React, {useEffect, useState} from 'react';

import Intro from './intro';
import Contact from './contact';
import Sale from './sale';
import {useParams} from 'react-router-dom';


const Index = () => {
    const [page, setPage] = useState();
    const {slug} = useParams();
    useEffect(() => {
        handleChangePage(slug);
    }, [slug]);
    const handleChangePage = (page, data = {}) => {
        setPage(page);
    };

    switch (page) {
        case 'intro':
            return <Intro/>;
        case 'contact':
            return <Contact/>;
        case 'sale':
            return <Sale/>
        default:
            return '';
    }
};

export default Index;
