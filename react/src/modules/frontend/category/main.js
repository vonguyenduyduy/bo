import React, {useEffect, useState} from 'react'
import {
    Segment,
    Grid,
    Image, Card, Divider, Label
} from 'semantic-ui-react';
import models from "../../../models/models";
import _ from 'lodash'
import constant from "../../../helpers/constant";
import utils from "../../../helpers/utils";
const Main = (props) => {
    const [product, setProduct] = useState([]);
    const {category, categoryId} = props;

    const parent = utils.findInArray(category, {_id: categoryId});
    console.log(categoryId);
    useEffect(() => {
        document.title = "Category";
        getList(categoryId);
    }, [categoryId]);
    async function getList(categoryId) {
        let rs  = await models.product.get({category: categoryId, status: constant.VAR.ACTIVE});
        setProduct(rs);
    }
    return (
        <div>
            <Segment raised>
                <Divider horizontal>
                    <Label as='a' color='teal' ribbon>
                        Products of {!_.isEmpty(parent) ? parent.title : ''}
                    </Label>
                </Divider>
            </Segment>
            <Grid doubling columns={4}>
                {
                    !_.isEmpty(product) && product.map((row, index) => (
                        <Grid.Column key={index}>
                            <Card>
                                <Image width="200" src={utils.parseImage(row.image)} size='small' wrapped />

                                <Card.Content>
                                    <Card.Header>{row.title}</Card.Header>
                                    <Card.Description color="red">
                                        <Label color="teal">
                                            {row.price}/Thùng/24Chai
                                        </Label>
                                    </Card.Description>
                                </Card.Content>
                            </Card>
                        </Grid.Column>
                    ))
                }
            </Grid>
        </div>
    )
}

export default Main