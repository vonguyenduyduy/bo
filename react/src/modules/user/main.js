import React from 'react';

import {Table, Button, Label, Image} from 'semantic-ui-react'
import models from '../../models/models'
import _ from 'lodash';

class Content extends React.Component{
    componentDidMount() {
        document.title = 'Admin Banner';
        this.getList();
    }
    state = {
        list : {}
    };
    getList = () => {
        const {model} = this.props;
        console.log(model);
        models[model].get().then((res) => {
            this.setState({
                list: res
            })
        })
    };
    deleteRow = (e, index) => {
        const {model} = this.props;

        models[model].delete({_id: index}).then((res) => {
            this.getList();
        })
    };
    toggleRow = (data) => {
        const {model} = this.props;

        models[model].put({_id: data._id}, {status: this.revertStatus(data.status)}).then((res) => {
            if (!res) {
                alert('toggleRow failed')
            } else {
                this.getList();
            }
        })
    };
    revertStatus = (status) => {
        return status === '1' ? '0' : '1';
    }

    render() {
        const {list} = this.state;
        const {handleChangePage} = this.props;
        return (
            <div className="content">
                <h2>User</h2>
                <Button icon="add" color='pink' onClick={() => handleChangePage('add')}/>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Username</Table.HeaderCell>
                            <Table.HeaderCell>Avatar</Table.HeaderCell>
                            <Table.HeaderCell>Token</Table.HeaderCell>
                            <Table.HeaderCell>Status</Table.HeaderCell>
                            <Table.HeaderCell>Action</Table.HeaderCell>

                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {
                            !_.isEmpty(list) && list.map((row, index) => {
                                return (
                                    <Table.Row negative={!(row.status === '1')} key={index}>
                                        <Table.Cell><Label size="big">{row.username}</Label></Table.Cell>
                                        <Table.Cell>
                                            <Image width="200" src={`${window.location.origin}/uploads/${row.image}`} size='small' wrapped />
                                        </Table.Cell>
                                        <Table.Cell><Label color="green">{row.token}</Label></Table.Cell>
                                        <Table.Cell>
                                            {
                                                row.status === '1' ? (
                                                    <Label as='span' color='green' tag>
                                                        Active
                                                    </Label>
                                                    ) : (
                                                    <Label as='span' tag>
                                                        Inactive
                                                    </Label>
                                                )
                                            }
                                        </Table.Cell>

                                        <Table.Cell collapsing textAlign='right'>
                                            <div>
                                                <Button onClick={(e) => this.toggleRow(row)} color={row.status === '1' ? 'green' : 'yellow'} icon="eye"/>
                                                <Button color='blue'>View</Button>
                                                <Button color='teal' onClick={() => handleChangePage('edit', row)}>Edit</Button>
                                                <Button onClick={(e) => this.deleteRow(e, row._id)} color='red'>Delete</Button>
                                            </div>
                                        </Table.Cell>
                                    </Table.Row>
                                )
                            })
                        }
                    </Table.Body>
                </Table>
            </div>
        );
    }
}

export default Content;
