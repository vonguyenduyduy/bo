import React from 'react';

import {Item, Grid, Segment, Header, Icon, Message} from 'semantic-ui-react'
import models from '../../models/models'
import _ from 'lodash';
import {Form, Input} from "formsy-semantic-ui-react";
import {io} from "socket.io-client";
const project = process.env.REACT_APP_PROJECT;
const socket = io(`ws://${process.env.REACT_APP_HOST}`);
const d = new Date();
const adminStyle = {
    textAlign: 'right'
}
class Content extends React.Component{

    constructor(props) {
        super(props)
        this.el = React.createRef()
    }
    componentDidUpdate = () => {
        this.scrollToBottom()
    };
    scrollToBottom = () => {
        if (!_.isEmpty(this.el.current)){
            this.el.current.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' })
        }
    };
    componentDidMount() {
        const d = new Date();
        console.log(new Date(d.setDate(d.getDate() -1)))
        document.title = 'Admin Chating';
        socket.on('chating',  (data) => {
            this.getList();
            this.scrollToBottom()
        });
        this.getList();
    }
    refs = {};
    state = {
        orgList: [],
        list : [],
        events: [],
        current: ''
    };
    getList = () => {
        const {model} = this.props;
        const {current} = this.state;

        models[model].get(
            {
                name:{ $exists: true, $ne:null},
                // dateTime: {$gte: new Date(d.setDate(d.getDate() -1)).toISOString()}
            }, {'name': -1}).then((res) => {
            let arr = [];
            const nRes = res.map((row) => {
                if (!_.includes(arr, row.name)) {
                    arr.push(row.name);
                    return row
                }
                return {};
            });
            const events = !_.isEmpty(current) ? _.filter(res, {name: current}): [];
            this.setState({
                orgList: res,
                list: _.reject(nRes, _.isEmpty),
                events: events
            }, () => {
                // console.log(this.state)
            })
        })
    };
    selectChat = ({name}) => {
        const {orgList} = this.state;
        const list = _.filter(orgList, {name: name});
        this.setState({
            current: name,
            events: list
        })
        this.scrollToBottom()

    };
    sendMessage = (message) => {
        let {current} = this.state;
        socket.emit('chating', {
            project: project,
            name: current,
            sender: 'admin',
            message: message,
            receiver: current
        });
    };
    onValidSubmit = (data) => {
            let {events, current} = this.state;
            this.setState({
                events: _.concat(events, {
                    name: current,
                    sender: 'admin',
                    receiver: current,
                    message: data.message,
                })
            });
            this.sendMessage(data.message);
            this.resetForm();
    };
    resetForm = () => {
        this.refs.form.reset();
    };
    render() {
        const {list, events, current} = this.state;
        const {handleChangePage} = this.props;
        return (
            <div className="content">
                <h2>Chat Manager</h2>
                <Grid divided>
                    <Grid.Column width={4}>
                        <Item.Group divided>
                            {!_.isEmpty(list) && list.map((row, index) => (
                                <Item onClick={() => this.selectChat(row)} key={index}>
                                    <Item.Content verticalAlign='middle'>
                                        <Segment>
                                        <Item.Header><Icon name="user circle"/> {row.name}</Item.Header>
                                        </Segment>
                                    </Item.Content>
                                </Item>
                            ))}
                        </Item.Group>
                    </Grid.Column>
                    <Grid.Column width={10}>
                        {!_.isEmpty(current) && (<Header><Icon size="huge" name="user circle"/> {current}</Header>)}
                        {
                            !_.isEmpty(current) && (
                                <Segment style={{backgroundColor: "#e4e4e4"}}>

                                    <Item.Group className="chat-over-frame">
                                        {!_.isEmpty(events) && events.map((row, index) => (
                                            <Item key={index}>

                                                <Item.Content style={row.sender === 'admin' ? adminStyle : {}}>
                                                    {row.sender !== 'admin' ? (<Icon color="teal" size="big" name="user circle"/>) : ''}

                                                    <Message success={(row.sender === 'admin')} compact className="chat-border">
                                                    <Item.Meta>
                                                        {row.sender}
                                                    </Item.Meta>

                                                    <Item.Description>
                                                        {row.message}
                                                    </Item.Description>
                                                    </Message>
                                                    {row.sender === 'admin' ? (<Icon color="yellow" size="big" name="smile"/>) : ''}

                                                </Item.Content>

                                            </Item>
                                        ))}
                                        <div ref={this.el} />
                                    </Item.Group>
                                    <Segment>
                                        <Form ref="form" onSubmit={this.onValidSubmit}>
                                            <Form.Field
                                                control={Input}
                                                label={'Admin on the Chat'}
                                                name="message"
                                                placeholder='Type here'
                                                width={14}
                                            />

                                        </Form>
                                    </Segment>
                                </Segment>
                            )
                        }

                    </Grid.Column>
                </Grid>
            </div>
        );
    }
}

export default Content;
