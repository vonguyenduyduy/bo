import React, { Component } from 'react'
import {
    Button,

} from 'semantic-ui-react'
import {
    Form, Input, Select
} from 'formsy-semantic-ui-react';
import models from '../../models/models';
import CONSTANT from '../../helpers/constant';
import _ from 'lodash';
import FileManager from "../../helpers/ui/FileManager";

class Detail extends Component {
    state = {
        imageName: {}
    };
    componentDidMount() {
        const {data} = this.props;
        this.setState({
            imageName: data.image
        });
    }

    onValidSubmit = async (data) => {
        const {handleChangePage, add, model} = this.props;
        const {imageName} = this.state;

        try{
            if (!_.isEmpty(imageName)) {
                data.image = imageName;
            }
            if (add) {
                models[model].post(data).then((res) => {
                    if (!res) {
                        alert('Created failed')
                    } else {
                        handleChangePage('main');
                    }
                })
            } else {

                models[model].put({_id: data._id}, _.omit(data, '_id')).then((res) => {
                    if (!res) {
                        alert('Update failed')
                    } else {
                        handleChangePage('main');
                    }
                })
            }
        } catch (e) {
            console.log(e)
        }

    };

    onHasClick = (dir) => {
        this.setState({
            imageName: dir
        })
    };

    render() {
        const {handleChangePage, data} = this.props;
        const {imageName} = this.state;

        return (
            <div>
                {!_.isEmpty(data) && (
                    <h2>{data._id}</h2>
                )}
                <Form size="big" onSubmit={this.onValidSubmit}>
                    <Form.Input name="_id" type="hidden" value={data._id}/>
                    <Form.Group>
                        <Form.Field
                            control={Input}
                            label='Title'
                            name="title"
                            placeholder='Title'
                            value={data.title}
                            width={8}
                        />
                        <Form.Field
                            control={Select}
                            name="status"
                            label='Status'
                            options={CONSTANT.OPTIONS.STATUS}
                            placeholder='Status'
                            width={3}
                            value={data.status}
                        />
                        <Form.Field width={5}>
                            <label>Image</label>
                            <FileManager image={imageName} action={false} onHasClick={this.onHasClick}/>
                        </Form.Field>
                    </Form.Group>

                    <Button color='grey' onClick={() => handleChangePage('main')}>Back</Button>
                    <Button color='teal' type="submit">Submit</Button>
                </Form>
            </div>

        )
    }
}

export default Detail