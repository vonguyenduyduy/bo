import React from 'react';

import 'semantic-ui-css/semantic.min.css'

import { Icon, Menu, Dropdown } from 'semantic-ui-react'
import { Link } from "react-router-dom";
import CONSTANT from '../../helpers/constant';
import lang from "../../helpers/lang";
import _ from "lodash";

class MenuC extends React.Component{
    state = { activeItem: 'gamepad' }

    handleItemClick = (e, { name }) => this.setState({ activeItem: name })
    render() {
        const { activeItem } = this.state;

        return (
            <div className="menu">

                <Menu icon='labeled' vertical>
                    {!_.isEmpty(process.user) && process.user.username === 'superadmin' && (
                        <>
                            <Menu.Item
                                as={Link}
                                name='gamepad'
                                active={activeItem === 'gamepad'}
                                onClick={this.handleItemClick}
                                to={CONSTANT.ROUTES.FE.HOME}
                            >
                                <Icon name='gamepad' />
                                Front Home
                            </Menu.Item>
                            <Menu.Item
                                as={Link}
                                name='banner'
                                active={activeItem === 'banner'}
                                onClick={this.handleItemClick}
                                to={CONSTANT.ROUTES.BE.BANNER}
                            >
                                <Icon name='adn' />
                                Banner
                            </Menu.Item>
                            <Menu.Item
                                as={Link}
                                name='user'
                                active={activeItem === 'user'}
                                onClick={this.handleItemClick}
                                to={CONSTANT.ROUTES.BE.USER}
                            >
                                <Icon name='user circle' />
                                BE User
                            </Menu.Item>
                        </>
                    )}

                    <Menu.Item
                        as={Link}
                        name='home'
                        active={activeItem === 'home'}
                        onClick={this.handleItemClick}
                        to={CONSTANT.ROUTES.BE.HOME}
                    >
                        <Icon name='home' />
                        Dashboard
                    </Menu.Item>
                    <Menu.Item
                        as={Link}
                        name='Category'
                        active={activeItem === 'Category'}
                        onClick={this.handleItemClick}
                        to={CONSTANT.ROUTES.BE.CATEGORY}
                    >
                        <Icon name='clipboard list' />
                        {lang('category')}
                    </Menu.Item>
                    <Menu.Item
                        as={Link}
                        name='product'
                        active={activeItem === 'product'}
                        onClick={this.handleItemClick}
                        to={CONSTANT.ROUTES.BE.PRODUCT}
                    >
                        <Icon name='recycle' />
                        {lang('product')}
                    </Menu.Item>

                    <Menu.Item
                        as={Link}
                        name='brand'
                        active={activeItem === 'brand'}
                        onClick={this.handleItemClick}
                        to={CONSTANT.ROUTES.BE.BRAND}
                    >
                        <Icon name='handshake outline' />
                        {lang('coop_brand')}
                    </Menu.Item>
                    <Menu.Item
                        as={Link}
                        name='news'
                        active={activeItem === 'news'}
                        onClick={this.handleItemClick}
                        to={CONSTANT.ROUTES.BE.NEWS}
                    >
                        <Icon name='adn' />
                        {lang('news')}
                    </Menu.Item>
                    <Menu.Item
                        as={Link}
                        name='news'
                        active={activeItem === 'news'}
                        onClick={this.handleItemClick}
                        to={CONSTANT.ROUTES.BE.FILE_MANAGER}
                    >
                        <Icon name='chess board ' />
                        {lang('file_manager')}
                    </Menu.Item>

                    <Menu.Item
                        name='setting'
                    >
                            <Dropdown item text='Setting'>
                            <Dropdown.Menu>
                                <Dropdown.Item as={Link} to={CONSTANT.ROUTES.BE.INTRO}>{lang('intro')}</Dropdown.Item>
                                <Dropdown.Item as={Link} to={CONSTANT.ROUTES.BE.CONTACT}>{lang('contact')}</Dropdown.Item>
                                <Dropdown.Item as={Link} to={CONSTANT.ROUTES.BE.SALE}>{lang('sale')}</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </Menu.Item>

                </Menu>

            </div>
        );
    }
}

export default MenuC;
