const language = {
    vi: {
        category: 'Danh mục sản phẩm',
        home: 'Trang chủ',
        intro: 'Giới thiệu',
        contact: 'Liên hệ',
        sale: 'Khuyến mãi',
        product_of: 'Sản phẩm thuộc danh mục: ',
        product: 'Sản phẩm',
        hot_product: 'Sản phẩm Hot',
        sale_product: 'Sản phẩm khuyến mãi',
        coop_brand: 'Đối tác - Thương hiệu',
        more: 'Xem thêm',
        news: 'Tin tức',
        file_manager: 'Quản lý hình ảnh',

    },
    en: {
        category: 'Category',
        home: 'Home'
    }
};
const lang = (key) => {
    let lag = process.lang ? process.lang : 'vi';
    return language[lag][key];
}

export default lang;