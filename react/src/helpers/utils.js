import _ from "lodash";
import lang from "./lang";
import Cookies from 'js-cookie'

const inObject = (array, field) => {
    return _.has(array, field);
};
const findInArray = (array, where) => {
    const index = _.findIndex(array, where);
    // console.log(array, where, index, 'index');
    if (index < 0) {
        return null;
    }
    return array[index];
}
const changLanguage = () => {
    process.lang = 'en';
    console.log(lang('category'))
};

const generateString = (length) => {
    var result           = [];
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result.push(characters.charAt(Math.floor(Math.random() *
            charactersLength)));
    }
    return result.join('');
}
const storeCookie = (key, data, minutes = 60) => {
    const expires = (60 * minutes) * 1000;
    const inOneHour = new Date(new Date().getTime() + expires);
    Cookies.set(key, data, { expires: inOneHour });
};
const getCookie = (key) => {
    return Cookies.get(key);
};
const parseImage = (uri) => {
    if (_.startsWith(uri, '/')) {
        uri = uri.substring(1)
    }
    return `${window.location.origin}/uploads/${uri}`
};
const utils = {
    inObject,
    findInArray,
    changLanguage,
    generateString,
    storeCookie,
    getCookie,
    parseImage
}
export default utils;