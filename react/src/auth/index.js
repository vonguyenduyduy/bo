import Cookies from 'js-cookie'
import models from '../models/models';

const getAccessToken = () => Cookies.get('access_token');
const setLogin = (tokens) => {
    const expires = (60 * 60) * 1000;
    const inOneHour = new Date(new Date().getTime() + expires);
    Cookies.set('access_token', tokens.access_token, { expires: inOneHour });
    Cookies.set('refresh_token', tokens.refresh_token)
};
const getRefreshToken = () => Cookies.get('refresh_token');
const isAuthenticated = () => !!getAccessToken();

const refreshTokens = async (data) => {
    // const rs = await models.user.get({username: data.username, password: data.password});
    // console.log(rs)
};
const setUser = async () => {
    const token = await getAccessToken();
    process.user = await models.user.getOne({access_token: token});
}
const authenticate = async () => {
    if (getRefreshToken()) {
        try {
            const tokens = await refreshTokens() ;// call an API, returns tokens

            const expires = (60 * 60) * 1000;
            const inOneHour = new Date(new Date().getTime() + expires);

            // you will have the exact same setters in your Login page/app too
            Cookies.set('access_token', tokens.access_token, { expires: inOneHour });
            Cookies.set('refresh_token', tokens.refresh_token);

            return true
        } catch (error) {
            redirectToLogin();
            return false
        }
    }

    redirectToLogin()
    return false
}

const redirectToLogin = () => {
    console.log(2222);
}
const logOut = () => {
    Cookies.remove('access_token');
    Cookies.remove('refresh_token');
    process.user = {};
};

const exp = {
    setLogin,
    isAuthenticated,
    authenticate,
    getAccessToken,
    setUser,
    logOut
};
export default exp;