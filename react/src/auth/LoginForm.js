import React, {useEffect, useState} from 'react'
import { Button, Grid, Message, Segment } from 'semantic-ui-react'
import {Form} from 'formsy-semantic-ui-react'
import _ from "lodash";
import models from "../models/models";
import constant from "../helpers/constant";
import utils from "../helpers/utils";
import auth from "../auth";
import {useHistory} from 'react-router-dom';

const LoginForm = () => {
    const [message, setMessage] = useState('');
    const history = useHistory();
    useEffect(() => {
        if (auth.isAuthenticated()) {
            history.push(constant.ROUTES.BE.HOME);
        }
        document.title = "Login Page";
    }, [])

    const onValidSubmit = async (data) => {
        if (data.username === 'superadmin' && data.password === 'zz') {
            auth.setLogin({...data, access_token: utils.generateString(64)});
            history.push(constant.ROUTES.BE.HOME);
        } else {
            const user = await models.user.getOne({...data, status: constant.VAR.ACTIVE});
            if (_.isEmpty(user)) {
                setMessage('Username or Password Invalid');
                return false;
            }
            auth.setLogin(user);
            history.push(constant.ROUTES.BE.HOME);
        }
    };

    return (
        <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
            <Grid.Column style={{ maxWidth: 450 }}>
                {!_.isEmpty(message) && (
                    <Message negative>
                        <Message.Header>{message}</Message.Header>
                    </Message>
                )}

                <Form onSubmit={onValidSubmit} size='large'>
                    <Segment stacked>
                        <Form.Input name="username" fluid icon='user' iconPosition='left' placeholder='Username' />
                        <Form.Input
                            name="password"
                            fluid
                            icon='lock'
                            iconPosition='left'
                            placeholder='Password'
                            type='password'
                        />

                        <Button type="submit" color='teal' fluid size='large'>
                            Login
                        </Button>
                    </Segment>
                </Form>

            </Grid.Column>
        </Grid>
    )
}

export default LoginForm