let utils = {
    inObject: function (array, field) {
        return _.has(array, field);
    }
};