$(function() {
  // Initialize variables
  const $get = $('#get'); // Input for username
  const $post = $('#post'); // Input for username
  const $put = $('#put'); // Messages area
  const $delete = $('#delete'); // Input message input box

  $get.click( async function () {
    let rs = await models.order.get();
    console.log(rs);
  });

  $post.click( async function () {
    let rs = await models.order.post({test: 1234455676889});
    await console.log(rs);
  });
  $delete.click( async function () {
    let rs = await models.order.delete({test: 1234455676889});
    await console.log(rs);
  });
});
