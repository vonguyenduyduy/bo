var express = require('express');
const app = express();
var server = require('http').createServer(app);
var port = process.env.PORT || 3000;
const _ = require('lodash');
var moment = require('moment'); // require

////

/////
const { MongoClient, ObjectId } = require("mongodb");

// DB
const MONGOOSE_URI = 'mongodb://mongo';

const client = new MongoClient(MONGOOSE_URI);
let dbCon;
const options = { upsert: true, new: true, setDefaultsOnInsert: true };


const socket = require('socket.io-client')('ws://nginx');

socket.on('connect', () => {
  console.log('connected to the socket');
  console.log(moment().format('YYYY-M-D HH:mm:ss'))
});

socket.on('disconnect', (reason) => {
  console.log(`disconnected due to ${reason}`);
});

socket.on('get', function ({table, unique, params, project, sort}) {
    try{
        if (_.has(params,'_id')) {
            params._id = ObjectId(params._id);
        }

        console.log(table, unique, params,sort, "#get");
        if (!_.isEmpty(sort)) {
            let rs = dbCon.db(project).collection(table).find(params).sort(sort).toArray().then((res) => {
                socket.emit('get_response', {unique: unique, result: true, data: res});
                return res;
            })
        } else {
            let rs = dbCon.db(project).collection(table).find(params).toArray().then((res) => {
                socket.emit('get_response', {unique: unique, result: true, data: res});
                return res;
            })
        }
    }catch (e) {
        socket.emit('get_response', {unique: unique, result: false, error: e});
        console.log(e);
    }
});

socket.on('post', function ({table, unique, params, project}) {
    try{
        dbCon.db(project).collection(table).insertOne(params, options, function (err, response) {
            if (err) {
                console.log('callback err', err);
                socket.emit('post_response', {unique: unique, result: false, error: err});
            } else {
                socket.emit('post_response', {unique: unique, result: true, data: response.ops});
                console.log('response err', response.ops);
            }
        });
    }catch (error) {
        console.log('catch', error);
        socket.emit('post_response', {unique: unique, result: false, error: error});
    }
});
socket.on('put', function ({table, unique, where, params, project}) {
    try{
        if (where._id) {
            where._id = ObjectId(where._id);
        }
        console.log(table, unique, where, params);
        dbCon.db(project).collection(table).updateOne(where, {$set: params}, { upsert: true}, function (err, response) {
            if (err) {
                console.log('callback err', err);
                socket.emit('put_response', {unique: unique, result: false, error: err.toString()});
            } else {
                socket.emit('put_response', {unique: unique, result: true});
                console.log('put successful');
            }
        });
    }catch (error) {
        console.log('catch', error);
        socket.emit('post_response', {unique: unique, result: false, error: error.toString()});
    }
});
socket.on('delete', function ({table, unique, params, project}) {
    try{
        console.log(table, unique, params, "abc delete");
        if (params._id) {
            params._id = ObjectId(params._id);
        }
        dbCon.db(project).collection(table).deleteOne(params, options, function (err, response) {
            if (err) {
                console.log('callback err', err);
                socket.emit('delete_response', {unique: unique, result: false, error: err});
            } else {
                socket.emit('delete_response', {unique: unique, result: true});
                console.log('response err', true);
            }
        });
    }catch (error) {
        console.log('catch', error);
        socket.emit('delete_response', {unique: unique, result: false, error: error});
    }
});

socket.on('chating',  (data) => {
    const d = new Date();

    const obj = {
        ...data,
        date: moment().format('YYYY-M-D'),
        dateTime: new Date(d.setDate(d.getDate() +2)),
    };

    dbCon.db(data.project).collection('chat').insertOne(obj);
});

server.listen(port,  async () => {
    const d = new Date();
    console.log("Client's started at:", d.toDateString(), ' --', d.toTimeString());
    await client.connect();
    dbCon = client;
});

// Routing

app.use(express.static(__dirname + '/public'));



