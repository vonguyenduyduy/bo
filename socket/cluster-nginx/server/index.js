// Setup basic express server
var express = require('express');
var app = express();

var server = require('http').createServer(app);
var io = require('socket.io')(server, {
    cors: {
        origin: '*',
    }
});
var redis = require('socket.io-redis');
var port = process.env.PORT || 3000;
var serverName = process.env.NAME || 'Unknown';

io.adapter(redis({ host: 'redis', port: 6379 }));

server.listen(port, function () {
  console.log('Server listening at port %d', port);
  console.log('Hello, I\'m %s, how can I help?', serverName);
});

// Routing
app.use(express.static(__dirname + '/public'));

// Chatroom

var numUsers = 0;

io.on('connection', function (socket) {
  socket.emit('my-name-is', serverName);

  var addedUser = false;

  // when the client emits 'new message', this listens and executes
  socket.on('new message', function (data) {
    // we tell the client to execute 'new message'
    socket.broadcast.emit('new message', {
      username: socket.username,
      message: data
    });
  });
  socket.on('chating', function (data) {
    // we tell the client to execute 'new message'
    socket.broadcast.emit('chating', data);
  });

  // when the client emits 'add user', this listens and executes
  socket.on('add user', function (username) {
    if (addedUser) return;

    // we store the username in the socket session for this client
    socket.username = username;
    ++numUsers;
    addedUser = true;
    socket.emit('login', {
      numUsers: numUsers
    });
    // echo globally (all clients) that a person has connected
    socket.broadcast.emit('user joined', {
      username: socket.username,
      numUsers: numUsers
    });
  });

  // when the client emits 'typing', we broadcast it to others
  socket.on('typing', function () {
    socket.broadcast.emit('typing', {
      username: socket.username
    });
  });

  // when the client emits 'stop typing', we broadcast it to others
  socket.on('stop typing', function () {
    socket.broadcast.emit('stop typing', {
      username: socket.username
    });
  });


  socket.on('add_order', function (data) {
    socket.broadcast.emit('add_order', data);
  });
  socket.on('get', function (data) {
    socket.broadcast.emit('get', data);
  });
  socket.on('get_response', function (data) {
    socket.broadcast.emit('get_response', data);
  });
  socket.on('post', function (data) {
    socket.broadcast.emit('post', data);
  });
  socket.on('post_response', function (data) {
    socket.broadcast.emit('post_response', data);
  });
  socket.on('put', function (data) {
    socket.broadcast.emit('put', data);
  });
  socket.on('put_response', function (data) {
    socket.broadcast.emit('put_response', data);
  });
  socket.on('delete', function (data) {
    socket.broadcast.emit('delete', data);
  });
  socket.on('delete_response', function (data) {
    socket.broadcast.emit('delete_response', data);
  });

  socket.on('file', function (data) {
    socket.broadcast.emit('file', data);
  });
  socket.on('file_response', function (data) {
    socket.broadcast.emit('file_response', data);
  });

  // when the user disconnects.. perform this
  socket.on('disconnect', function () {
    console.log('disconnect');
  });
});
